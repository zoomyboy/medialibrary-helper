# Laravel Medialibrary Helper

This package creates routes for the popular Medialibrary Package from Spatie ().

## Available methods

In RegisterMediaCollections, you have the following methods available:

You can set a filename by default for the file. This accepts the associated Model, as well as the original filename. You should return the new name of the file with the extension (e.g. disc.jpg).

```
forceFileName(fn ($model, $path) => Str::slug($path))
```

You can set a max width (in Pixels) for images. This will resize the image BEFORE any normal Medialibrary conversions take place.

```
maxWidth(fn () => 2500)
```

You can call whatever you want after an image has been added, modified or deleted.

```
->after(function ($model) {
    ....
})
```

