<?php

namespace Zoomyboy\MedialibraryHelper;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function register(): void
    {
        app()->bind('media-library-helpers', fn () => collect([]));
        app()->singleton(CollectionExtension::class, fn () => new CollectionExtension());
    }

    public function boot(): void
    {
        app(Router::class)->group($this->routeGroup(), function ($router) {
            $router->post('mediaupload', [MediaController::class, 'store'])->name('media.store');
            $router->delete('mediaupload/{media}', [MediaController::class, 'destroy'])->name('media.destroy');
            $router->get('mediaupload/{parent_model}/{parent_id}/{collection_name}', [MediaController::class, 'index'])->name('media.index');
            $router->patch('mediaupload/{parent_model}/{parent_id}/{collection_name}', OrderController::class)->name('media.order');
            $router->patch('mediaupload/{media}', [MediaController::class, 'update'])->name('media.update');
        });

        app(CollectionExtension::class)->boot();
    }

    /**
     * @return array{middleware: array<int, string>}
     */
    protected function routeGroup(): array
    {
        return [
            'middleware' => config('media-library.middleware'),
        ];
    }
}
