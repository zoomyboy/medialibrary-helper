<?php

namespace Zoomyboy\MedialibraryHelper\Tests;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Gate;
use Orchestra\Testbench\TestCase as BaseTestCase;
use Spatie\LaravelData\LaravelDataServiceProvider;
use Spatie\MediaLibrary\MediaLibraryServiceProvider;
use Zoomyboy\MedialibraryHelper\ServiceProvider;
use Zoomyboy\MedialibraryHelper\Tests\Models\Post;

class TestCase extends BaseTestCase
{
    /**
     * Define database migrations.
     */
    protected function defineDatabaseMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
    }

    protected function getPackageProviders($app): array
    {
        return [
            ServiceProvider::class,
            MediaLibraryServiceProvider::class,
            LaravelDataServiceProvider::class,
        ];
    }

    /**
     * Generate a pdf file with a filename and get path.
     */
    protected function pdfFile(?string $filename = null): File
    {
        return $this->getFile('pdf.pdf', $filename ?: 'pdf.pdf');
    }

    protected function jpgFile(?string $filename = null): File
    {
        return $this->getFile('jpg.jpg', $filename ?: 'jpg.jpg');
    }

    protected function getFile(string $location, string $as): File
    {
        $path = __DIR__ . '/stubs/' . $location;
        $to = sys_get_temp_dir() . '/' . $as;
        copy($path, $to);

        return new File($to);
    }

    protected function registerModel(): static
    {
        app()->extend('media-library-helpers', fn ($p) => $p->put('post', Post::class));

        return $this;
    }

    protected function newPost(): Post
    {
        return Post::create(['title' => 'Lorem', 'content' => 'aafff']);
    }

    protected function auth(array $policies = []): self
    {
        $policies = [
            'storeMedia' => true,
            'updateMedia' => true,
            'destroyMedia' => true,
            'listMedia' => true,
            ...$policies,
        ];

        foreach ($policies as $ability => $result) {
            Gate::define($ability, fn (?string $user, string $collectionName) => $result);
        }

        return $this;
    }

    protected function defineEnvironment($app)
    {
        $app['config']->set('media-library.middleware', ['web']);
    }
}
