<?php

namespace Zoomyboy\MedialibraryHelper\Tests\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Spatie\MediaLibrary\HasMedia;

class MediaDestroyed
{
    use Dispatchable;
    use SerializesModels;

    public function __construct(public HasMedia $model)
    {
    }

    public function broadcastOn()
    {
        return [];
    }
}
