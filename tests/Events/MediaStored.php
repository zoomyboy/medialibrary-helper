<?php

namespace Zoomyboy\MedialibraryHelper\Tests\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MediaStored
{
    use Dispatchable;
    use SerializesModels;

    public function __construct(public Media $media)
    {
    }

    public function broadcastOn()
    {
        return [];
    }
}
