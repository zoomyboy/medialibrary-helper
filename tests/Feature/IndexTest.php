<?php

namespace Zoomyboy\MedialibraryHelper\Tests\Feature;

use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

test('it gets all medias', function () {
    $this->auth()->registerModel();
    $post = $this->newPost();
    $firstMedia = $post->addMedia($this->pdfFile()->getPathname())->withCustomProperties(['test' => 'old'])->preservingOriginal()->toMediaCollection('images');
    $secondMedia = $post->addMedia($this->pdfFile()->getPathname())->withCustomProperties(['test' => 'old'])->preservingOriginal()->toMediaCollection('images');

    $response = $this->getJson("/mediaupload/post/{$post->id}/images");

    $response->assertStatus(200);
    $response->assertJsonPath('0.id', $firstMedia->id);
    $response->assertJsonPath('1.id', $secondMedia->id);
    $response->assertJsonPath('1.properties.test', 'old');
    $response->assertJsonPath('1.icon', url('storage/filetypes/applicationpdf.svg'));
});

test('it gets media in order', function () {
    $this->auth()->registerModel();
    $post = $this->newPost();
    $firstMedia = $post->addMedia($this->pdfFile()->getPathname())->withCustomProperties(['test' => 'old'])->preservingOriginal()->toMediaCollection('images');
    $secondMedia = $post->addMedia($this->pdfFile()->getPathname())->withCustomProperties(['test' => 'old'])->preservingOriginal()->toMediaCollection('images');
    $thirdMedia = $post->addMedia($this->pdfFile()->getPathname())->withCustomProperties(['test' => 'old'])->preservingOriginal()->toMediaCollection('images');
    $order = $post->getMedia('images')->pluck('id');
    $order->prepend($order->pop());
    Media::setNewOrder($order->toArray());

    $response = $this->getJson("/mediaupload/post/{$post->id}/images");

    $response->assertStatus(200);
    $response->assertJsonPath('0.id', $thirdMedia->id);
    $response->assertJsonPath('1.id', $firstMedia->id);
    $response->assertJsonPath('2.id', $secondMedia->id);
});

test('it gets media for single', function () {
    $this->auth()->registerModel();
    $post = $this->newPost();
    $media = $post->addMedia($this->pdfFile()->getPathname())->withCustomProperties(['test' => 'old'])->preservingOriginal()->toMediaCollection('defaultSingleFile');

    $response = $this->getJson("/mediaupload/post/{$post->id}/defaultSingleFile");

    $response->assertStatus(200);
    $response->assertJsonPath('id', $media->id);
    $response->assertJsonPath('properties.test', 'old');
    $response->assertJsonPath('fallback', false);
    $response->assertJsonPath('mime_type', 'application/pdf');
});

test('it checks for authorization', function () {
    $this->auth(['listMedia' => false])->registerModel();
    $post = $this->newPost();
    $post->addMedia($this->pdfFile()->getPathname())->withCustomProperties(['test' => 'old'])->preservingOriginal()->toMediaCollection('images');

    $response = $this->getJson("/mediaupload/post/{$post->id}/images");

    $response->assertStatus(403);
});

test('it returns 404 when media not found', function () {
    $this->auth()->registerModel();
    $post = $this->newPost();

    $response = $this->getJson("/mediaupload/post/{$post->id}/defaultSingleFile");

    $response->assertStatus(404);
});

test('test it gets conversions for single media', function () {
    $this->auth()->registerModel();
    $post = $this->newPost();
    $media = $post->addMedia($this->jpgFile()->getPathname())->preservingOriginal()->toMediaCollection('conversionsWithDefault');

    $response = $this->getJson("/mediaupload/post/{$post->id}/conversionsWithDefault");

    $response->assertStatus(200);
    $response->assertJsonPath('conversions.tiny.original_url', $media->getFullUrl('tiny'));
});

test('test it gets default single media', function () {
    $this->auth()->registerModel();
    $post = $this->newPost();
    Storage::disk('public')->put('default.jpg', $this->jpgFile()->getContent());

    $response = $this->getJson("/mediaupload/post/{$post->id}/conversionsWithDefault");

    $response->assertStatus(200);
    $response->assertJsonPath('id', null);
    $response->assertJsonPath('original_url', Storage::disk('public')->url('default.jpg'));
    $response->assertJsonPath('name', 'default');
    $response->assertJsonPath('file_name', 'default.jpg');
    $response->assertJsonPath('fallback', true);
    $response->assertJsonPath('mime_type', 'image/jpeg');
});
