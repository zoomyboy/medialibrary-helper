<?php

namespace Zoomyboy\MedialibraryHelper\Tests\Feature;

use Illuminate\Support\Facades\Event;
use Zoomyboy\MedialibraryHelper\Tests\Events\MediaChange;

test('it can reorder media', function () {
    Event::fake();
    $this->auth()->registerModel();
    $post = $this->newPost();
    $post->addMedia($this->pdfFile()->getPathname())->preservingOriginal()->toMediaCollection('images');
    $post->addMedia($this->pdfFile()->getPathname())->preservingOriginal()->toMediaCollection('images');
    $post->addMedia($this->pdfFile()->getPathname())->preservingOriginal()->toMediaCollection('images');
    $order = $post->getMedia('images')->pluck('id');
    $order->prepend($order->pop());

    $response = $this->patchJson("/mediaupload/post/{$post->id}/images", [
        'order' => $order,
    ]);

    $response->assertStatus(200);
    $response->assertJsonPath('0.id', $order->get(0));
    $response->assertJsonPath('1.id', $order->get(1));
    $response->assertJsonPath('2.id', $order->get(2));
    $this->assertEquals($order, $post->fresh()->getMedia('images')->pluck('id'));
    Event::assertDispatched(MediaChange::class, fn ($event) => $event->model->is($post));
});

test('images should belong to same model', function () {
    $this->auth()->registerModel();

    $post = $this->newPost();
    $firstMedia = $post->addMedia($this->pdfFile()->getPathname())->preservingOriginal()->toMediaCollection('images');

    $post = $this->newPost();
    $secondMedia = $post->addMedia($this->pdfFile()->getPathname())->preservingOriginal()->toMediaCollection('images');
    $thirdMedia = $post->addMedia($this->pdfFile()->getPathname())->preservingOriginal()->toMediaCollection('images');

    $response = $this->patchJson("/mediaupload/post/{$post->id}/images", [
        'order' => [$firstMedia->id, $secondMedia->id, $thirdMedia->id],
    ]);

    $response->assertJsonValidationErrors('order');
});

test('it should authorize', function () {
    $this->auth(['listMedia' => false])->registerModel();

    $post = $this->newPost();
    $media = $post->addMedia($this->pdfFile()->getPathname())->preservingOriginal()->toMediaCollection('images');

    $response = $this->patchJson("/mediaupload/post/{$post->id}/images", [
        'order' => [$media->id],
    ]);

    $response->assertStatus(403);
});
