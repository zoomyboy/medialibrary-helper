<?php

namespace Zoomyboy\MedialibraryHelper\Tests\Feature;

use Illuminate\Support\Facades\Event;
use Zoomyboy\MedialibraryHelper\Tests\Events\MediaChange;
use Zoomyboy\MedialibraryHelper\Tests\Events\MediaDestroyed;

test('it deletes multiple media', function () {
    $this->auth()->registerModel()->withoutExceptionHandling();
    $post = $this->newPost();
    $post->addMedia($this->pdfFile()->getPathname())->withCustomProperties(['test' => 'old'])->preservingOriginal()->toMediaCollection('multipleForced');
    $post->addMedia($this->pdfFile()->getPathname())->withCustomProperties(['test' => 'old'])->preservingOriginal()->toMediaCollection('multipleForced');
    $media = $post->getFirstMedia('multipleForced');

    $this->deleteJson("/mediaupload/{$media->id}")->assertStatus(200);

    $this->assertCount(1, $post->fresh()->getMedia('multipleForced'));
});

test('it deletes single media', function () {
    $this->auth()->registerModel();
    $post = $this->newPost();
    $post->addMedia($this->pdfFile()->getPathname())->withCustomProperties(['test' => 'old'])->preservingOriginal()->toMediaCollection('defaultSingleFile');
    $media = $post->getFirstMedia('defaultSingleFile');

    $this->deleteJson("/mediaupload/{$media->id}")->assertStatus(200);

    $this->assertCount(0, $post->fresh()->getMedia('defaultSingleFile'));
});

test('it needs authorization', function () {
    $this->auth(['destroyMedia' => false])->registerModel();
    $post = $this->newPost();
    $post->addMedia($this->pdfFile()->getPathname())->withCustomProperties(['test' => 'old'])->preservingOriginal()->toMediaCollection('defaultSingleFile');
    $media = $post->getFirstMedia('defaultSingleFile');

    $this->deleteJson("/mediaupload/{$media->id}")->assertStatus(403);
});

test('it fires event', function () {
    Event::fake();
    $this->auth()->registerModel()->withoutExceptionHandling();
    $post = $this->newPost();
    $post->addMedia($this->pdfFile()->getPathname())->preservingOriginal()->toMediaCollection('singleWithEvent');
    $media = $post->getFirstMedia('singleWithEvent');

    $this->deleteJson("/mediaupload/{$media->id}")->assertStatus(200);

    Event::assertDispatched(MediaDestroyed::class, fn ($event) => $event->model->is($post));
    Event::assertDispatched(MediaChange::class, fn ($event) => $event->model->is($post));
});
