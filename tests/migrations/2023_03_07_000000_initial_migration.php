<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class InitialMigration extends Migration {

    public function up(): void
    {
        Schema::create('posts', function($table) {
            $table->id();
            $table->string('title');
            $table->string('content');
            $table->timestamps();
        });
    }
}
